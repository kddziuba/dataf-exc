#!/bin/bash -x

apt-get install software-properties-common -y
apt-add-repository ppa:ansible/ansible
apt-get update
apt-get install ansible -y
echo "localhost ansible_connection=local" > /etc/ansible/hosts
ansible-playbook /ansible/playbook.yml

/usr/sbin/apache2ctl -D FOREGROUND
exit
