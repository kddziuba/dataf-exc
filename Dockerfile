FROM ubuntu:latest

RUN apt-get update
RUN apt-get -y upgrade

EXPOSE 8080

# Copy files into place.
ADD docker-src /

# Run bootstrap script with ansible
RUN chmod +x /bootstrap.sh
CMD /bootstrap.sh 

